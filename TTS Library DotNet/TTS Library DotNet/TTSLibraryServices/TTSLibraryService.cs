﻿
using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TTSLibraryServices
{
    public class TTSLibraryService
    {
        private Socket IOSocket;
        public string ServiceURL { get; set; }

        AutoResetEvent ConnectionEvent = new AutoResetEvent(false);

        AutoResetEvent ResultEvent = new AutoResetEvent(false);

        private ResultCommand result;

        private bool isConnected = false;


        public TTSLibraryService(string serviceURL)
        {
            ServiceURL = serviceURL;
        }

        internal bool CreateFile(string text, string voice, string path, int volume, int rate, string filename)
        {
            GenerateTTSCommand command = new GenerateTTSCommand()
            {
                Text = text,
                Voice = voice,
                Path = path,
                Volume = volume, 
                Rate = rate,
                Filename = filename
            };

            Console.WriteLine("hasta aca");
            try
            {
                IOSocket = IO.Socket(ServiceURL);

                IOSocket.On(Socket.EVENT_CONNECT, OnConnection);
                IOSocket.On("result", OnAudioCreationComplete);

                ConnectionEvent.WaitOne(2000);
                /*
                if (!isConnected)
                {
                    Console.WriteLine("Connection timeout");
                    return false;
                }*/
                IOSocket.Emit("generateTTS", "holi");//JsonConvert.SerializeObject(command));

                ResultEvent.WaitOne(5000);

                IOSocket.Disconnect();
                if (result != null)
                {
                    return result.Success;
                }

                Console.WriteLine("Result timeout");
                return false;
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            //IOSocket = null;
            
        }

        internal void OnAudioCreationComplete(object data)
        {
            //Console.WriteLine(data);
            result = JsonConvert.DeserializeObject<ResultCommand>(data.ToString());
            result = new ResultCommand();
            ResultEvent.Set();
        }

        internal void OnConnection()
        {
            Console.WriteLine("connected");
            isConnected = true;
            ConnectionEvent.Set();
        }

        internal void OnCreationComplete()
        {
            Console.WriteLine("OnCreationComplete");

        }
    }
}
