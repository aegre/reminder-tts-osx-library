﻿using NameSpaceLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TTSLibraryServices
{
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    [ComVisible(true)]
    [Guid("8FC4E1C1-85D9-4EEE-A831-3A76EF074257")]
    public interface ILibraryTTSMethods
    {
        [DispId(100)]
        bool CreateFile(string text, string voice, string path, string serviceURL, int volume, int rate, string filename);
    }




    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("Tts.OSX.Library")]
    [Guid("FCD39A2F-9852-4A2C-9783-6B1998C6E6FB")]
    public class TTSOSXLibrary : ILibraryTTSMethods
    {
        public bool CreateFile(string text, string voice, string path, string serviceURL, int volume, int rate, string filename)
        {
            Logger myLogger;
            myLogger = new Logger("MSPLog");
            myLogger.open();
            myLogger.log("MainClass -> .ctor");
            myLogger.log("V2");
            Console.WriteLine("aqui");
            try
            {
                TTSLibraryService service = new TTSLibraryService(serviceURL);
                return service.CreateFile(text, voice, path, volume, rate, filename);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                myLogger.log("Exception");
                myLogger.log(e.Message);
                myLogger.log(e.StackTrace);
                return false;
            }
        }
    }
}
