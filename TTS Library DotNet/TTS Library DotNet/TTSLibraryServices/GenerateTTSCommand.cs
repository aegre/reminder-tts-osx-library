﻿using Newtonsoft.Json;

namespace TTSLibraryServices
{
    public class GenerateTTSCommand
    {

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "rate")]
        public int Rate { get; set; }

        [JsonProperty(PropertyName = "voice")]
        public string Voice { get; set; }

        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }

        [JsonProperty(PropertyName ="volume")]
        public int Volume { get; internal set; }

        [JsonProperty(PropertyName ="filename")]
        public string Filename { get; internal set; }
    }
}

