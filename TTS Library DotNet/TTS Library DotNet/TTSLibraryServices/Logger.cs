﻿using System;
using System.IO;

namespace NameSpaceLogger
{
    public class Logger
    {
        private FileStream theFileStream = null;
        private StreamWriter theStreamWriter = null;

        string theFilename = "";

        DateTime ahorita;

        private string port = "P_000";

        object syncRoot = new object();

        public string Port
        {
            set
            {
                port = value;
            }
        }

        //metodos
        public Logger(string fileName)
        {
            theFilename = fileName;
        }
        public void open()
        {
            try
            {
                //checa directorio
                if (!Directory.Exists(@".\logsMSP"))
                    Directory.CreateDirectory(@".\logsMSP");

                //calcula
                ahorita = DateTime.Now;

                //abre archvio
                theFileStream = new FileStream(fileName(theFilename, ahorita), FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                theStreamWriter = new StreamWriter(theFileStream);
            }
            catch (Exception ex)
            {
                FileStream theFileStreamException = null;
                StreamWriter theStreamWriterException = null;

                ahorita = DateTime.Now;

                theFileStreamException = new FileStream(fileName(theFilename + "_Exception_", ahorita), FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                theStreamWriterException = new StreamWriter(theFileStreamException);

                String exceptionFull;
                exceptionFull = String.Format("{2} {0:HH:mm:ss.fff} ***LoggerException->open() {1}", DateTime.Now, ex.ToString(), port);

                theStreamWriterException.WriteLine(exceptionFull);

                theStreamWriterException.Flush();

                if (theStreamWriterException != null)
                {
                    theStreamWriterException.Dispose();
                    theStreamWriterException.Close();
                }

                if (theFileStreamException != null)
                {
                    theFileStreamException.Dispose();
                    theFileStreamException.Close();
                }
            }
        }

        ~Logger()
        {
            closeStreams();
        }

        public void closeStreams()
        {
            try
            {
                if (theStreamWriter != null)
                {
                    theStreamWriter.Dispose();
                    theStreamWriter.Close();
                }

                if (theFileStream != null)
                {
                    theFileStream.Dispose();
                    theFileStream.Close();
                }
            }
            catch (Exception ex)
            {

                FileStream theFileStreamException = null;
                StreamWriter theStreamWriterException = null;

                ahorita = DateTime.Now;

                theFileStreamException = new FileStream(fileName(theFilename + "_Exception_", ahorita), FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                theStreamWriterException = new StreamWriter(theFileStreamException);

                String exceptionFull;
                exceptionFull = String.Format("{2} {0:HH:mm:ss.fff} ***LoggerException->closeStreams() {1}", DateTime.Now, ex.ToString(), port);

                theStreamWriterException.WriteLine(exceptionFull);

                theStreamWriterException.Flush();

                if (theStreamWriterException != null)
                {
                    theStreamWriterException.Dispose();
                    theStreamWriterException.Close();
                }

                if (theFileStreamException != null)
                {
                    theFileStreamException.Dispose();
                    theFileStreamException.Close();
                }
            }
        }

        public void log(string info)
        {
            lock (syncRoot)
            {

                String fullLine;
                fullLine = String.Format("{2} {0:HH:mm:ss.fff} {1}", DateTime.Now, info, port);
                try
                {
                    if (theStreamWriter != null)
                    {
                        theStreamWriter.WriteLine(fullLine);
                        theStreamWriter.Flush();
                    }
                    else
                        Console.WriteLine(fullLine);
                }
                catch (Exception ex)
                {
                    FileStream theFileStreamException = null;
                    StreamWriter theStreamWriterException = null;

                    ahorita = DateTime.Now;

                    theFileStreamException = new FileStream(fileName(theFilename + "_Exception_", ahorita), FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                    theStreamWriterException = new StreamWriter(theFileStreamException);

                    String exceptionFull;
                    exceptionFull = String.Format("{2} {0:HH:mm:ss.fff} ***LoggerException->log() {1}", DateTime.Now, ex.ToString(), port);

                    theStreamWriterException.WriteLine(fullLine);
                    theStreamWriterException.WriteLine(exceptionFull);

                    theStreamWriterException.Flush();

                    if (theStreamWriterException != null)
                    {
                        theStreamWriterException.Dispose();
                        theStreamWriterException.Close();
                    }

                    if (theFileStreamException != null)
                    {
                        theFileStreamException.Dispose();
                        theFileStreamException.Close();
                    }
                }
            }

        }

        private string fileName(string _theFileName, DateTime _ahorita)
        {
            string fileName = @".\logsMSP\" + _theFileName + _ahorita.ToString("yyyyMMdd") + ".log";
            return fileName;
        }

    }
}
